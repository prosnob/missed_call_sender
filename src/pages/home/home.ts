import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {HttpClient} from '@angular/common/http';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AlertController } from 'ionic-angular';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  isVerified:boolean = false;
  phoneData: any;
  phone:string='';
  constructor(public navCtrl: NavController,
  public http: HttpClient,
  private iab: InAppBrowser,
  private alertCtrl: AlertController) {
    this.enterPhone();
  }

  //ask user to input phone number to verify
  enterPhone() {
    let alert = this.alertCtrl.create({
      title: 'Enter your number',
      inputs: [
        {
          name: 'phone',
          placeholder: 'phone number',
          type: 'tel',
          value:this.phone,
        }
      ],
      buttons: [
        {
          text: 'Save',
          handler: data => {
            if(data.phone === ''){
              return false;
            }else{
              this.phone = data.phone;
            }
          }
        }
      ],
      enableBackdropDismiss: false
    });
    alert.present();
  }

  makeACall(){
    //get out of app to phone call
    this.iab.create('tel:+85585478643');
    const interval = setInterval(
      ()=>{
        try {
          let time = Date.now();
          //check phone number verify from db
          this.http.get('http://192.168.88.177:3000/missed-call/' + this.phone + '/' + time).subscribe(data => {
            this.phoneData = data;
          }, err => {
            console.log(err);
          });
          if(this.phoneData.length !== 0){
            clearInterval(interval);
            // update phone number to verified on db
            this.http.patch('http://192.168.88.177:3000/missed-call/update/' + this.phoneData[0].id,{status:'VERIFIED'}).subscribe(data => {
             this.phoneData = [];
             console.log(data);
            }, err => {
             console.log(err);
            });
            this.isVerified = true;
            console.log(this.isVerified);
          }
        } catch (error) {}
      },1000
    )
  }
}
